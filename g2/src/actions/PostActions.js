export const getPost = (postId) => {
  return (dispatch) => {
    dispatch({
      type: 'LOAD_POST_START'
    });

    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
        method: 'GET'
      })
      .then((response) => {
        response.json().then((data) => {

          if (response.status === 200) {
            let post = data;

            dispatch({
              type: 'LOAD_POST_SUCCESS',
              payload: post
            });

            return;
          }

          dispatch({
            type: 'LOAD_POST_ERROR',
            payload: 'Something bad happened'
          });
        });
      });
  };
};