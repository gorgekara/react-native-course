export const getUser = () => {
  return (dispatch) => {
    dispatch({
      type: 'LOAD_USER_START'
    });

    fetch('https://randomuser.me/api/', {
        method: 'GET'
      })
      .then((response) => {
        response.json().then((data) => {

          if (response.status === 200) {
            let user = data.results[0];

            dispatch({
              type: 'LOAD_USER_SUCCESS',
              payload: user
            });

            return;
          }

          dispatch({
            type: 'LOAD_USER_ERROR',
            payload: 'Something bad happened'
          });
        });
      });
  };
};