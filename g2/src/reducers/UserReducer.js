const INITIAL_STATE = {
  loading: false,
  user: {},
  message: ''
};

export default (state = INITIAL_STATE, action) => {

  switch (action.type) {

    case 'LOAD_USER_START':
      return { ...state, loading: true };

    case 'LOAD_USER_SUCCESS':
      return { ...state, loading: false, user: action.payload };

    case 'LOAD_USER_ERROR':
      return { ...state, loading: false, message: action.payload };

    default:
      return state;

  }

};