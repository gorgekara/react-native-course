const INITIAL_STATE = {
  loading: false,
  post: {},
  message: ''
};

export default (state = INITIAL_STATE, action) => {

  switch (action.type) {

    case 'LOAD_POST_START':
      return { ...state, loading: true };

    case 'LOAD_POST_SUCCESS':
      return { ...state, loading: false, post: action.payload };

    case 'LOAD_POST_ERROR':
      return { ...state, loading: false, message: action.payload };

    default:
      return state;

  }

};