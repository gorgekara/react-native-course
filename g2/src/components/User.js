import React, { Component } from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';

import { getUser } from '../actions/UserActions';
import Post from './Post';

class User extends Component {

  startLoading() {
    this.props.getUser();
  }

  render() {
    let { name } = this.props.user;

    return (
      <View>
        <Text>{ this.props.loading ? 'Loading..' : '' }</Text>

        <Text>NAME: { name ? name.first : '' }</Text>
        <Text>SURNAME: { name ? name.last : '' }</Text>

        <TouchableHighlight onPress={this.startLoading.bind(this)}>
          <Text>Load</Text>
        </TouchableHighlight>

        <Post id="1" />
        <Post id="5" />
        <Post id="88" />
      </View>
    );
  }

}

const mapStateToProps = (state) => {
  const { loading, user, message } = state.user;
  return { loading, user, message };
};

export default connect(mapStateToProps, { getUser })(User);