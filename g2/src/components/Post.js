import React, { Component } from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';

import { getPost } from '../actions/PostActions';

class Post extends Component {

  componentDidMount() {
    this.props.getPost(this.props.id);
  }

  render() {
    let post = this.props.post;

    return (
      <View>
        <Text>{ post ? post.title : '' }</Text>
      </View>
    );
  }

}

const mapStateToProps = (state, ownProps) => {
  const { post } = state.post;
  const { id } = ownProps;
  
  return { post, id };
};

export default connect(mapStateToProps, { getPost })(Post);