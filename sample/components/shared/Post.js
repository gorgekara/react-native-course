import React, { Component } from 'react';
import { Text, Linking, View, TouchableHighlight } from 'react-native';

export default class Post extends Component {

  constructor(props) {
    super(props);
  }

  goto(param) {
    
    // Linking gives you a general interface to interact with both
    // incoming and outgoing app links.
    // More info: https://facebook.github.io/react-native/docs/linking.html
    Linking.openURL(`http://google.com/search?q=${param}`).catch(err => {
      // handle error
    });
    
  }

  render() {
    let post = this.props.data || [];

    return (
      <View style={styles.post}>
        <Text style={styles.title}>{post.title}</Text>
        <Text>{post.body}</Text>
        <TouchableHighlight style={styles.btn} onPress={this.goto.bind(this, post.title)}>
          <Text style={styles.btnText}>Search</Text>
        </TouchableHighlight>
      </View>
    );
  }

}

const styles = {
  post: {
    marginBottom: 10,
    flex: 0.5
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  btn: {
    marginTop: 10,
    padding: 10,
    backgroundColor: '#0088FF'
  },
  btnText: {
    color: '#FFF',
    textAlign: 'center'
  }
};