//////////
// If we export multiple functions or variables from
// a file than we can use ES6 destructuring
// to import them easily.
// More info: http://stackoverflow.com/questions/33524696/es6-destructuring-and-module-imports
//////////////
import React, { Component } from 'react';
import { TouchableHighlight, TextInput, Alert, View, ScrollView, Text, ActivityIndicator, Image, AsyncStorage, Dimensions } from 'react-native';

import Post from './shared/Post';

//////////
// React Native components can be declared:
// - as class
// - as functions
//////////////


//////////
// React Native component declared as class
// (extends the React.Component)
//////////////
export default class Box extends Component {

  //////////
  // Props passed to components declared as classes
  // must be passed to the constructor and called
  // with super. Otherwise there will be an error shown.
  //////////////
  constructor(props) {
    super(props);

    this.state = {
      name: props.name,
      posts: []
    };
  }

  changeName() {
    this.setState({
      name: 'Pepito'
    });
  }

  async componentWillMount() {
    // AsyncStorage is a simple, unencrypted, asynchronous, persistent,
    // key-value storage system that is global to the app.
    // It should be used instead of LocalStorage.
    // More info: https://facebook.github.io/react-native/docs/asyncstorage.html
    let posts = await AsyncStorage.getItem('posts');

    if (posts) {
      this.setState({
        posts: JSON.parse(posts)
      });

      alert('Already in storage');

      return;
    }

    //////////
    // For creating AJAX requests in React Native
    // we use fetch. Fetch requires it's first param
    // to be the URL to which we are making the request
    // while an object for the second one.
    // More info: https://facebook.github.io/react-native/docs/network.html
    //////////////
    fetch('https://jsonplaceholder.typicode.com/posts', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      alert('Fetched from API');

      response.json().then((data) => {
        AsyncStorage.setItem('posts', JSON.stringify(data), () => {});

        this.setState({
          posts: data
        });
      });

    });
  }

  openAlert() {
    Alert.alert(
      'Hello World',
      'My Alert Msg',
      [
        { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false }
    );
  }

  //////////
  // All components declared as class
  // must have a render method in them.
  //////////////
  render() {
    return (
      <ScrollView style={styles.boxWrap}>

        {
          // Usage of image - takes in prop source
          // More info: https://facebook.github.io/react-native/docs/images.html
        }
        <Image style={styles.image} source={require('../images/dexter.jpg')} />

        <Text>Width: {Dimensions.get('window').width}</Text>
        <Text>Height: {Dimensions.get('window').height}</Text>

        <TouchableHighlight onPress={this.openAlert.bind(this)}>
          <Text>Open Alert</Text>
        </TouchableHighlight>

        {
          // TextInput is a basic component that allows the user to enter text.
          // It has an onChangeText prop that takes a function to be called
          // every time the text changed, and an onSubmitEditing prop that
          // takes a function to be called when the text is submitted.
          // More info: https://facebook.github.io/react-native/docs/handling-text-input.html
        }
        <TextInput
          style={{ height: 40 }}
          placeholder="Type your text here"
          onChangeText={(text) => this.setState({ text })} />
        <Text>{this.state.text}</Text>

        <ActivityIndicator animating={!this.state.posts.length} />

        {
          this.state.posts.map((post, index) => {
            return <Post key={index} data={post}/>
          })
        }

      </ScrollView>
    );
  }
}

//////////
// React Native component declared as function
//////////////
// export default ({ name }) => {
//   this.state = {
//     name: name
//   };

//   return (
//     <View>
//       <Text style={styles.boxTitle}>Box Test Function</Text>
//       <Text>Name: {this.state.name}</Text>
//     </View>
//   );
// };

const styles = {
  boxWrap: {
    marginBottom: 20,
    flexDirection: 'column'
  },
  boxTitle: {
    fontWeight: 'bold',
    fontSize: 20
  },
  btn: {
    marginTop: 10,
    padding: 10,
    backgroundColor: '#0088FF'
  },
  btnText: {
    color: '#FFF',
    textAlign: 'center'
  },
  image: {
    resizeMode: 'contain',
    width: null,
    height: 200
  }
};