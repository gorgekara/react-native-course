import React from 'react';
import { Provider } from 'react-redux';
import { Scene, Router, ActionConst, DefaultRenderer } from 'react-native-router-flux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';

import reducers from './reducers';
import User from './components/User';
import Test from './components/Test';
import SideDrawer from './components/SideDrawer';

const App = () => {
  const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

  return (
    <Provider store={store}>
      <Router>
        <Scene key="drawer" component={SideDrawer} open={false} initial>
          <Scene key="home" component={User} initial />
          <Scene key="test" component={Test} />
        </Scene>
      </Router>
    </Provider>
  );
};

export default App;