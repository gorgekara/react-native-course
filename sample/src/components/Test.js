import React, { Component } from 'react';
import { View, Text, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';

export default class Test extends Component {

  openMenu() {
    Actions.refresh({ key: 'drawer', open: true })
  }

  render() {
    return (
      <View style={{backgroundColor: '#FFF', flex: 1, paddingTop: 50 }}>
        <TouchableHighlight onPress={this.openMenu}>
          <Text>Open Menu</Text>
        </TouchableHighlight>

        <Text>Test Screen</Text>
      </View>
    );
  }

}