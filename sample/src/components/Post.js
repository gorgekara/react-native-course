import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableHighlight } from 'react-native';

import { changePost } from '../actions/PostActions';

class Post extends Component {

  changeMe() {
    this.props.changePost(this.props.data.id);
  }

  render() {
    let postStyle = { height: 50, marginTop: 20, backgroundColor: '#EEE' };

    if (this.props.data.colored) {
      postStyle['backgroundColor'] = 'red';
    }

    return (
      <View style={postStyle}>
        <Text>{ this.props.data ? this.props.data.title : '' }</Text>
        <Text>Colored: { this.props.data.colored ? 'Yes' : 'No' }</Text>
        <TouchableHighlight onPress={this.changeMe.bind(this)}>
          <Text>Change</Text>
        </TouchableHighlight>
      </View>
    );
  }

}

const mapStateToProps = (state, ownProps) => {
  const { data } = ownProps;
  return { data };
};

export default connect(mapStateToProps, { changePost })(Post);