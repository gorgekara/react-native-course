import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ListView, Text, TouchableHighlight, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Post from './Post';
import { getUser } from '../actions/UserActions';
import { getPosts } from '../actions/PostActions';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2});

class User extends Component {

  componentDidMount() {
    this.fetchUser();
  }

  fetchUser() {
    this.props.getUser();
    this.props.getPosts();
  }

  openMenu() {
    Actions.refresh({ key: 'drawer', open: true })
  }

  render() {
    let { name, picture } = this.props.user;

    return (
      <View style={{backgroundColor: '#FFF', paddingTop: 50}}>
        <TouchableHighlight onPress={this.openMenu}>
          <Text>Open Menu</Text>
        </TouchableHighlight>
        <ListView 
          enableEmptySections={true}
          dataSource={this.props.listPosts}
          renderRow={(rowData, sectionID, rowId) => {
            console.log('Rendering Row');
            return (<Post data={rowData} />);
          }} />
      </View>
    );
  }

}

const mapStateToProps = (state) => {
  const { user } = state.user;
  const { posts } = state.post;


  let listPosts = ds.cloneWithRows(posts);

  return { user, listPosts };
};

export default connect(mapStateToProps, { getUser, getPosts })(User);