import React, { Component } from 'react';
import Drawer from 'react-native-drawer';
import { View, Text, Button } from 'react-native';
import { Actions, DefaultRenderer } from 'react-native-router-flux';

export default class SideDrawer extends Component {
  onClose() {
    Actions.refresh({ key: 'drawer', open: false });
  }

  onTestPress() {
    Actions.test({ type: 'reset' });
    Actions.refresh({ key: 'drawer', open: false });
  }

  onHomePress() {
    Actions.home({ type: 'reset' });
    Actions.refresh({ key: 'drawer', open: false });
  }

  render() {
    const state = this.props.navigationState || false;
    const children = state.children;

    return (
      <Drawer
        ref="navigation"
        open={state.open}
        type="static"
        side="right"
        content={
          <View style={{ marginBottom: 30 }}>
            <Button title="Home" onPress={this.onHomePress} />
            <Button title="Test" onPress={this.onTestPress}/>
          </View>
        }
        styles={drawerStyles}
        tapToClose={true}
        onClose={this.onClose.bind(this)}
        openDrawerOffset={0.2}>
          <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate} />
      </Drawer>
    );
  }
}

const drawerStyles = {
  drawer: {
    shadowColor: '#000000',
    shadowOpacity: 0.8,
    shadowRadius: 3
  },
  main: {
    paddingLeft: 0
  }
};