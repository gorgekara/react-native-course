const INITIAL_STATE = {
  user: {},
  message: '',
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case 'LOAD_USER':
      return { ...state, loading: true };

    case 'LOAD_USER_SUCCESS':
      return { ...state, loading: false, user: action.payload, message: 'User loaded successfully!' };

    case 'LOAD_USER_ERROR':
      return { ...state, loading: false, message: 'User could not load. Please try again later!' };

    default:
      return state;
  }
};