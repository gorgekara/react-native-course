const INITIAL_STATE = {
  post: {},
  posts: [],
  message: '',
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case 'LOAD_POST':
      return { ...state, loading: true };

    case 'LOAD_POST_SUCCESS':
      return { ...state, loading: false, post: action.payload, message: 'Post loaded successfully!' };

    case 'LOAD_POST_ERROR':
      return { ...state, loading: false, message: 'Post could not load. Please try again later!' };


    case 'LOAD_POSTS':
      return { ...state, loading: true };

    case 'LOAD_POSTS_SUCCESS':
      return { ...state, loading: false, posts: action.payload, message: 'Post loaded successfully!' };

    case 'LOAD_POSTS_ERROR':
      return { ...state, loading: false, message: 'Post could not load. Please try again later!' };


    case 'CHANGE_POST':
      let newPosts = state.posts.map((post) => {
        return post.id === action.payload ? { ...post, colored: true } : post;
      });
      
      return {
        ...state,
        posts: newPosts
      }

    default:
      return state;
  }
};