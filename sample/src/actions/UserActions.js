export const getUser = () => {
  return (dispatch) => {
    dispatch({
      type: 'LOAD_USER'
    });

    fetch('https://randomuser.me/api')
      .then((response) => {
        response.json().then((data) => {
          if (response.status >= 200 && response.status <= 300) {
            dispatch({
              type: 'LOAD_USER_SUCCESS',
              payload: data.results[0]
            });

            return;
          }

          dispatch({
            type: 'LOAD_USER_ERROR'
          });
        });
      });
  };
};