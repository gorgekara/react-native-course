export const getPost = (id) => {
  return (dispatch) => {
    dispatch({
      type: 'LOAD_POST'
    });

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then((response) => {
        response.json().then((data) => {
          if (response.status >= 200 && response.status <= 300) {
            dispatch({
              type: 'LOAD_POST_SUCCESS',
              payload: data
            });

            return;
          }

          dispatch({
            type: 'LOAD_POST_ERROR'
          });
        });
      });
  };
};

export const getPosts = () => {
  return (dispatch) => {
    dispatch({
      type: 'LOAD_POSTS'
    });

    fetch(`https://jsonplaceholder.typicode.com/posts`)
      .then((response) => {
        response.json().then((data) => {
          if (response.status >= 200 && response.status <= 300) {
            dispatch({
              type: 'LOAD_POSTS_SUCCESS',
              payload: data
            });

            return;
          }

          dispatch({
            type: 'LOAD_POSTS_ERROR'
          });
        });
      });
  };
};

export const changePost = (postId) => {
  return {
    type: 'CHANGE_POST',
    payload: postId
  }
};