import React, { Component } from 'react';
import { AppRegistry, Text, View } from 'react-native';

//////////
// Include the App component in the ./src folder.
// Do not add the extension of the file (ex.: '.js').
//////////////
import App from './src/App';


//////////
// Create a class named 'sample' which inherits the
// React.Component class. Make sure to export it as default
// to make it avialable when importing it in other files.
//////////////
export default class sample extends Component {
  render() {
    return (
      <App />
    );
  }
}

AppRegistry.registerComponent('sample', () => sample);
