import React, { Component } from 'react';
import { AppRegistry, Text, View } from 'react-native';

import App from './src/App';

export default class g1 extends Component {

  render() {
    return (
      <View style={styles.container}>
        <App />
      </View>
    );
  }

}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
};

AppRegistry.registerComponent('g1', () => g1);
