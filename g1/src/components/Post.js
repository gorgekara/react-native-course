import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { connect } from 'react-redux';

import { getPost } from '../actions/PostActions';

class Post extends Component {

  componentDidMount() {
    this.props.getPost(this.props.postId);
  }

  render() {
    let { title } = this.props.post;

    return (
      <Text>POST: { title }</Text>
    );
  }

}

const mapStateToProps = (state, ownProps) => {
  const { post } = state.post;

  return { post: post, postId: ownProps.postId };
};

export default connect(mapStateToProps, { getPost })(Post);