import React, { Component } from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';

import { getTempUser } from '../actions/UserActions';
import Post from './Post';

class User extends Component {

  loadUser() {
    this.props.getTempUser();
  }

  render() {
    let { name } = this.props.user;

    return (
      <View>
        <Text>{ this.props.loading ? 'Loading...' : '' }</Text>
        
        <Text>Name: { name ? name.first : '' }</Text>
        <Text>Last: { name ? name.last : '' }</Text>

        <TouchableHighlight onPress={this.loadUser.bind(this)}>
          <Text>RANDOM</Text>
        </TouchableHighlight>
      </View>
    );
  }

}

const mapStateToProps = (state) => {
  const { loading, user } = state.user;
  return { loading, user };
};

export default connect(mapStateToProps, { getTempUser })(User);