export const getUser = () => {
  return (dispatch) => {
    
    dispatch({
      type: 'LOAD_USER_START'
    });

    fetch('https://randomuser.me/api/')
      .then((response) => {
        response.json().then((data) => {
          if (response.status === 200) {
            dispatch({
              type: 'LOAD_USER_SUCCESS',
              payload: data.results[0]
            });

            return;
          }

          dispatch({
            type: 'LOAD_USER_ERROR'
          });
        });
      });

  };
};

export const getTempUser = () => {
  return (dispatch) => {
    dispatch(getUser());
  };
};