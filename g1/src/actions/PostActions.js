export const getPost = (postId) => {
  return (dispatch) => {
    
    dispatch({
      type: 'LOAD_POST_START'
    });

    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`)
      .then((response) => {
        response.json().then((data) => {
          if (response.status === 200) {
            dispatch({
              type: 'LOAD_POST_SUCCESS',
              payload: data
            });

            return;
          }

          dispatch({
            type: 'LOAD_POST_ERROR'
          });
        });
      });

  };
};