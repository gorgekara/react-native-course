const INITIAL_STATE = {
  loading: false,
  user: {},
  message: ''
};

export default (state = INITIAL_STATE, action) => {

  switch (action.type) {

    case 'LOAD_USER_START':
      return { ...state, loading: true };

    case 'LOAD_USER_SUCCESS':
      return { ...state, user: action.payload, loading: false };

    case 'LOAD_USER_ERROR':
      return { ...state, message: 'Something bad happened!', loading: false };

    default:
      return state;
  }

};