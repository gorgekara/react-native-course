import { combineReducers } from 'redux';

import UserReducer from './UserReducer';
import PostReducer from './PostReducer';

export default combineReducers({
  user: UserReducer,
  post: PostReducer
});