const INITIAL_STATE = {
  loading: false,
  post: {},
  message: ''
};

export default (state = INITIAL_STATE, action) => {

  switch (action.type) {

    case 'LOAD_POST_START':
      return { ...state, loading: true };

    case 'LOAD_POST_SUCCESS':
      return { ...state, post: action.payload, loading: false };

    case 'LOAD_POST_ERROR':
      return { ...state, message: 'Something bad happened!', loading: false };

    default:
      return state;
  }

};